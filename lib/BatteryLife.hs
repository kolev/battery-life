{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module BatteryLife where

import           Control.Applicative              ((<|>))
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString                  hiding (putStrLn)
import           Data.ByteString.Char8            (putStrLn)
import           Data.Monoid                      ((<>))
import           Data.String.Conversions          (cs)
import           Prelude                          hiding (concat, putStrLn)
import           System.Process                   (shell)
import           System.Process.ByteString        (readCreateProcessWithExitCode)

data ChargingState = Charged
                   | Discharging
                   | Charging
                   | FinishingCharge
                   deriving (Show)

data BatteryState = BatteryState
    { bsChargePercentage      :: Int
    , bsChargingState         :: ChargingState
    , bsTimeRemainingEstimate :: Maybe ByteString
    } deriving (Show)

parseBatteryState :: Parser BatteryState
parseBatteryState = do
    _ <- parseSourceName
    p <- parsePercentage
    _ <- string "; "
    s <- parseChargingState
    _ <- string "; "
    e <- parseEstimate
    return $ BatteryState p s e
  where
    parsePercentage :: Parser Int
    parsePercentage = read <$> many1 digit <* char '%'

    parseChargingState :: Parser ChargingState
    parseChargingState =   ("charged"          >> return Charged)
                       <|> ("discharging"      >> return Discharging)
                       <|> ("charging"         >> return Charging)
                       <|> ("finishing charge" >> return FinishingCharge)

    parseSourceName :: Parser String
    parseSourceName = manyTill anyChar (char '\t')

    parseEstimate :: Parser (Maybe ByteString)
    parseEstimate = option Nothing (Just <$> parseTime)

    parseTime :: Parser ByteString
    parseTime = do
        h <- takeWhile1 isDigit
        s <- ":"
        m <- takeWhile1 isDigit
        return $ concat [h, s, m]

convertPmsetOutput :: ByteString -> ByteString
convertPmsetOutput bs =
    either (const "??") showBatteryState $ parseOnly parseBatteryState bs

showBatteryState :: BatteryState -> ByteString
showBatteryState BatteryState{..} =
    (cs . show) bsChargePercentage <> "%" <> estimate
  where
    estimate = case bsTimeRemainingEstimate of
        Nothing -> mempty
        Just e  -> if e == "0:00" then mempty else " (" <> dir <> e <> ")"

    dir = case bsChargingState of
              Discharging -> mempty
              _           -> "+"

main :: IO ()
main = do
    let cmd = shell "pmset -g batt | grep InternalBattery"
    (_, stdout, _) <- readCreateProcessWithExitCode cmd ""
    putStrLn $ convertPmsetOutput stdout

