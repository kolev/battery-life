{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import           Test.Tasty       (defaultMain, testGroup)
import           Test.Tasty.HUnit

import qualified BatteryLife      as BL (convertPmsetOutput)


main :: IO ()
main = defaultMain $ testGroup "Battery Life Parser Tests" $
    map (\(n, i, o) -> testCase n $ BL.convertPmsetOutput i @?= o)
        [
          ( "Charged"
          , " -InternalBattery-0\t100%; charged; 0:00 remaining"
          , "100%"
          )
        ,
          ( "Finishing charge"
          , " -InternalBattery-0\t99%; finishing charge; 0:08 remaining"
          , "99% (+0:08)"
          )
        ,
          ( "Charging"
          , " -InternalBattery-0\t59%; charging; 1:03 remaining"
          , "59% (+1:03)"
          )
        ,
          ( "Discharging (no estimate)"
          , " -InternalBattery-0\t100%; discharging; (no estimate)"
          , "100%"
          )
        ,
          ( "Discharging"
          , " -InternalBattery-0\t5%; discharging; 0:15 remaining"
          , "5% (0:15)"
          )
        ]

